
Pod::Spec.new do |spec|
  spec.name         = 'TrebelConnect'
  spec.version      = '0.0.7'
  spec.summary      = 'TrebelConnect by Trebel.'

  spec.description      = <<-DESC
  Connect your app to Trebel to control Trebel music app from outside.
  DESC

  spec.homepage     = "https://home.trebel.io/"
  spec.license      = { :type => 'MIT', :file => 'LICENSE' }
  spec.author       = { 'MNM Media' => 'mnm.nahatakyan@gmail.com' }

  spec.ios.deployment_target = "12"
  spec.swift_version = "5"

  # spec.source        = { :git => "https://gitlab.com/Nahatakyan/trebelconnect", :tag => "#{spec.version}" }
  # spec.source_files  = 'TrebelConnect/**/*'
  spec.source       = { :http => "https://gitlab.com/Nahatakyan/trebelconnect/uploads/e1820f0d38e1cdb8fa41a81a441a3c1e/TrebelConnect.framework.zip" }
  spec.vendored_frameworks = "TrebelConnect.framework"
  spec.pod_target_xcconfig = { 'ONLY_ACTIVE_ARCH' => 'YES' }

  spec.dependency 'WebRTC-lib', '96.0.0'
  spec.dependency 'Socket.IO-Client-Swift', '16.0.0'
end
