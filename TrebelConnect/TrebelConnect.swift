//
//  TrebelConnect.swift
//  TrebelConnect
//
//  Created by Ruben Nahatakyan on 02.06.22.
//

import UIKit
import WebRTC

@objc public protocol TrebelConnectDelegate: NSObjectProtocol {
    @objc optional func trebelConnectStateChanged(to state: TrebelConnectState)
    @objc optional func trebelConnectReceivedTrackInfo(model: TrebelTrackInfo)
    @objc optional func trebelConnectProgressChanged(to progress: CGFloat)
    @objc optional func trebelConnectPlayerStateChanged(to state: TrebelPlayerState)
}

@objc open class TrebelConnect: NSObject {
    // MARK: - Properties
    @objc public static let shared = TrebelConnect()
    @objc public var state: TrebelConnectState = .disconnected {
        didSet {
            TrebelConnectLog.log("State changed: \(String(describing: state))")
            DispatchQueue.main.async { [self] in
                delegates.allObjects.forEach { $0.trebelConnectStateChanged?(to: state) }
            }
        }
    }

    private var roomID: String = ""

    private var webRTCClient: WebRTCClient!
    private lazy var socketClient: SocketClient? = {
        let client = SocketClient(room: roomID)
        client.delegate = self
        return client
    }()

    var logEnabled: Bool {
        #if DEBUG
            return true
        #else
            return false
        #endif
    }

    private var delegates = NSHashTable<TrebelConnectDelegate>.weakObjects()

    private var connectionPlayType: TrebelConnectPlayType = .shuffle

    private var backgroundTaskIdentifier: UIBackgroundTaskIdentifier = .invalid

    // MARK: - Init
    override private init() {
        super.init()
        addNotificationListeners()
    }
}

// MARK: - Actions
private extension TrebelConnect {
    func addNotificationListeners() {
        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillResignActive(_:)), name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive(_:)), name: UIApplication.didBecomeActiveNotification, object: nil)
    }

    @objc func applicationWillResignActive(_ sender: Notification) {
        guard let application = sender.object as? UIApplication else { return }
        backgroundTaskIdentifier = application.beginBackgroundTask(expirationHandler: {
            application.endBackgroundTask(self.backgroundTaskIdentifier)
            self.backgroundTaskIdentifier = .invalid
        })
    }

    @objc func applicationDidBecomeActive(_ sender: Notification) {
        guard backgroundTaskIdentifier != .invalid, let application = sender.object as? UIApplication else { return }
        application.endBackgroundTask(backgroundTaskIdentifier)
        backgroundTaskIdentifier = .invalid
    }

    func parseData(_ dictionary: [AnyHashable: Any]) {
        guard let typeValue = dictionary["data_type"] as? String, let type = TrebelConnectDataType(rawValue: typeValue), let info = dictionary["data"] as? [AnyHashable: Any] else {
            TrebelConnectLog.log("Parse data error")
            return
        }

        switch type {
        case .trackInfo:
            do {
                let data = try JSONSerialization.data(withJSONObject: info, options: .fragmentsAllowed)
                let model = try JSONDecoder().decode(TrebelTrackInfo.self, from: data)

                DispatchQueue.main.async { [self] in
                    delegates.allObjects.forEach { $0.trebelConnectReceivedTrackInfo?(model: model) }
                }

            } catch {
                TrebelConnectLog.log("Error decode \(typeValue): \(error)")
            }
        case .playerAction:
            break
        case .playerProgress:
            do {
                let data = try JSONSerialization.data(withJSONObject: info, options: .fragmentsAllowed)
                let model = try JSONDecoder().decode(PlayerProgressModel.self, from: data)

                DispatchQueue.main.async { [self] in
                    delegates.allObjects.forEach { $0.trebelConnectProgressChanged?(to: model.progress) }
                }

            } catch {
                TrebelConnectLog.log("Error decode \(typeValue): \(error)")
            }
        case .playerState:
            do {
                let data = try JSONSerialization.data(withJSONObject: info, options: .fragmentsAllowed)
                let model = try JSONDecoder().decode(PlayerStateModel.self, from: data)

                DispatchQueue.main.async { [self] in
                    delegates.allObjects.forEach { $0.trebelConnectPlayerStateChanged?(to: model.state) }
                }

            } catch {
                TrebelConnectLog.log("Error decode \(typeValue): \(error)")
            }
        }
    }

    func sendActionToTrebel(_ action: PlayerActionType) {
        guard webRTCClient.connectionState == .connected else { return }
        sendPlayerAction(action)
    }

    func sendPlayerAction(_ action: PlayerActionType) {
        do {
            let model = PlayerAction(type: action)
            let track = try JSONSerialization.jsonObject(with: try JSONEncoder().encode(model)) as? [AnyHashable: Any] ?? [:]
            let socketModel: [AnyHashable: Any] = [
                "data_type": TrebelConnectDataType.playerAction.rawValue,
                "data": track
            ]

            let data = try JSONSerialization.data(withJSONObject: socketModel)
            webRTCClient?.sendData(data)
        } catch {
            TrebelConnectLog.log("WebRTC send data error: \(error)")
        }
    }
}

// MARK: - Socket delegate
extension TrebelConnect: SocketDelegate {
    func socketClientConnected(socket: SocketClient) {}

    func socketClientDisconnected(socket: SocketClient) {}

    func socketClientReceiveOffer(socket: SocketClient, sdp: RTCSessionDescription, from: String) {
        addWebRTCClient(with: from, room: socket.room)
        guard let client = webRTCClient else { return }
        client.set(remoteSdp: sdp) { error in
            if let error = error {
                TrebelConnectLog.log("WebRTC set offer sdp error: \(error)")
                return
            }

            client.answer { sdp in
                socket.send(event: "answer", sdp: sdp, to: client.socketId)
            }
        }
    }

    func socketClientReceiveAnswer(socket: SocketClient, sdp: RTCSessionDescription, from: String) {
        webRTCClient?.set(remoteSdp: sdp) { error in
            guard let error = error else { return }
            TrebelConnectLog.log("WebRTC set answer sdp error: \(error)")
        }
    }

    func socketClientReceiveCandidate(socket: SocketClient, candidate: RTCIceCandidate, from: String) {
        webRTCClient?.set(remoteCandidate: candidate) { error in
            guard let error = error else { return }
            TrebelConnectLog.log("WebRTC set candidate error: \(error)")
        }
    }

    func socketClientReceiveOtherUser(socket: SocketClient, sid: String) {
        addWebRTCClient(with: sid, room: socket.room)
        webRTCClient?.offer { sdp in
            socket.send(event: "offer", sdp: sdp, to: sid)
        }
    }

    func addWebRTCClient(with socketId: String, room: String) {
        if let client = webRTCClient, client.connectionState == .connected {
            client.close()
        }
        webRTCClient = WebRTCClient(socketId: socketId, room: room)
        webRTCClient?.delegate = self
    }

    func sendData(data: Data, to client: SocketClient) {
        webRTCClient?.sendData(data)
    }
}

// MARK: - WebRTC delegate
extension TrebelConnect: WebRTCClientDelegate {
    func webRTCClient(_ client: WebRTCClient, didDiscoverLocalCandidate candidate: RTCIceCandidate) {
        socketClient?.send(candidate: candidate, to: client.socketId)
    }

    func webRTCClient(_ client: WebRTCClient, didChangeConnectionState state: RTCIceConnectionState) {
        TrebelConnectLog.log("WebRTC client did change state: \(state)")
    }

    func webRTCClient(_ client: WebRTCClient, didReceiveData data: Data) {
        TrebelConnectLog.log("WebRTC client did receive data: \(data.text)")
        let dictionary = (try? JSONSerialization.jsonObject(with: data) as? [AnyHashable: Any]) ?? [:]
        parseData(dictionary)
    }

    func webRTCClientOpenedNewChannel(_ client: WebRTCClient) {
        socketClient?.disconnect(completion: nil)
        socketClient = nil

        state = .connected
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [self] in
            play(with: connectionPlayType)
            connectionPlayType = .shuffle
        }
    }
}

// MARK: - Public methods
public extension TrebelConnect {
    @objc func connect(with type: TrebelConnectPlayType = .shuffle) throws {
        guard state == .disconnected else { throw TrebelConnectError.AlreadyConnectedOrConnecting }

        connectionPlayType = type
        roomID = UUID().uuidString.replacingOccurrences(of: "-", with: "")
        guard
            let url = URL(string: "trebel://peer-to-peer?token=\(roomID)"),
            UIApplication.shared.canOpenURL(url)
        else { throw TrebelConnectError.CantOpenTrebelApp }

        state = .connecting
        socketClient?.connect()
        UIApplication.shared.open(url)
    }

    @objc func addDelegate(_ delegate: TrebelConnectDelegate) {
        delegates.add(delegate)
    }

    @objc func removeDelegate(_ delegate: TrebelConnectDelegate) {
        guard delegates.contains(delegate) else { return }
        delegates.remove(delegate)
    }

    @objc func play() {
        sendActionToTrebel(.play)
    }

    @objc func pause() {
        sendActionToTrebel(.pause)
    }

    @objc func next() {
        sendActionToTrebel(.next)
    }

    @objc func previous() {
        sendActionToTrebel(.previous)
    }

    @objc func play(with type: TrebelConnectPlayType) {
        switch type {
        case .shuffle:
            sendActionToTrebel(.shuffle)
        case .mostPlayed:
            sendActionToTrebel(.mostPlayed)
        case .recentlyPlayed:
            sendActionToTrebel(.recentlyPlayed)
        }
    }
}
