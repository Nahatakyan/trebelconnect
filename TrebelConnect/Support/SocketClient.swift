//
//  SocketClient.swift
//  TrebelConnectTest
//
//  Created by Ruben Nahatakyan on 31.05.22.
//

import Foundation
import WebRTC
import SocketIO

protocol SocketDelegate: NSObjectProtocol {
    func socketClientConnected(socket: SocketClient)
    func socketClientDisconnected(socket: SocketClient)
    func socketClientReceiveOffer(socket: SocketClient, sdp: RTCSessionDescription, from: String)
    func socketClientReceiveAnswer(socket: SocketClient, sdp: RTCSessionDescription, from: String)
    func socketClientReceiveCandidate(socket: SocketClient, candidate: RTCIceCandidate, from: String)
    func socketClientReceiveOtherUser(socket: SocketClient, sid: String)
}

class SocketClient: NSObject {
    // MARK: - Properties
    private let manager: SocketManager
    private let socket: SocketIO.SocketIOClient

    weak var delegate: SocketDelegate?

    public let room: String

    // MARK: - Init
    init(room: String) {
        self.room = room

        self.manager = SocketManager(socketURL: URL(string: "https://dev-p2p.projectcarmen.com")!, config: [
            .log(true),
            .compress,
            .forceWebsockets(true)
        ])
        self.socket = self.manager.defaultSocket

        super.init()
        addListeners()
    }
}

// MARK: - Actions
private extension SocketClient {
    func addListeners() {
        self.socket.on(clientEvent: .connect) { [weak self] _, _ in
            TrebelConnectLog.log("Socket connected \(self?.socket.sid ?? "")")
            guard let `self` = self  else { return }
            self.delegate?.socketClientConnected(socket: self)
            self.joinRoom()
        }

        self.socket.on(clientEvent: .disconnect) { [weak self] _, _ in
            TrebelConnectLog.log("Socket disconnected")
            guard let `self` = self else { return }
            self.delegate?.socketClientDisconnected(socket: self)
        }

        self.socket.on(clientEvent: .error) { data, _ in
            TrebelConnectLog.log("Socket received error \(data)")
            debugPrint(#function, #line, self.socket)
        }

        self.socket.on("other-user") { [weak self] data, _ in
            guard let `self` = self, let sid = data.first as? String else { return }
            TrebelConnectLog.log("Socket received other user: \(sid)")
            self.delegate?.socketClientReceiveOtherUser(socket: self, sid: sid)
        }

        self.socket.on("offer") { [weak self] data, _ in
            guard
                let `self` = self,
                let dictionary = data.first as? [AnyHashable: Any],
                let caller = dictionary["caller"] as? String,
                let target = dictionary["target"] as? String,
                target == self.socket.sid,
                let sdp = dictionary["sdp"] as? [AnyHashable: Any]
            else { return }
            TrebelConnectLog.log("Socket received offer: target - \(target), caller - \(caller)")

            do {
                let data = try JSONSerialization.data(withJSONObject: sdp, options: .prettyPrinted)
                let sessionDescription = try JSONDecoder().decode(SessionDescription.self, from: data)
                self.delegate?.socketClientReceiveOffer(socket: self, sdp: sessionDescription.rtcSessionDescription, from: caller)
            } catch {
                TrebelConnectLog.log("Socket offer error decode: \(error)")
                return
            }
        }

        self.socket.on("answer") { [weak self] data, _ in
            guard
                let `self` = self,
                let dictionary = data.first as? [AnyHashable: Any],
                let caller = dictionary["caller"] as? String,
                let target = dictionary["target"] as? String,
                target == self.socket.sid,
                let sdp = dictionary["sdp"] as? [AnyHashable: Any]
            else { return }
            TrebelConnectLog.log("Socket received answer: target - \(target), caller - \(caller)")

            do {
                let data = try JSONSerialization.data(withJSONObject: sdp, options: .prettyPrinted)
                let sessionDescription = try JSONDecoder().decode(SessionDescription.self, from: data)
                self.delegate?.socketClientReceiveAnswer(socket: self, sdp: sessionDescription.rtcSessionDescription, from: caller)
            } catch {
                TrebelConnectLog.log("Socket answer error decode: \(error)")
                return
            }
        }

        self.socket.on("ice-candidate") { [weak self] data, _ in
            guard
                let `self` = self,
                let dictionary = data.first as? [AnyHashable: Any],
                let caller = dictionary["caller"] as? String,
                let target = dictionary["target"] as? String,
                target == self.socket.sid,
                let candidate = dictionary["candidate"] as? [AnyHashable: Any]
            else { return }
            TrebelConnectLog.log("Socket received candidate: target - \(target), caller - \(caller)")

            do {
                let data = try JSONSerialization.data(withJSONObject: candidate, options: .prettyPrinted)
                let iceCandidate = try JSONDecoder().decode(IceCandidate.self, from: data)
                self.delegate?.socketClientReceiveCandidate(socket: self, candidate: iceCandidate.rtcIceCandidate, from: caller)
            } catch {
                TrebelConnectLog.log("Socket candidate error decode: \(error)")
                return
            }
        }
    }
}

// MARK: - Actions
private extension SocketClient {
    func joinRoom() {
        let dictionary = [
            "roomId": self.room,
            "client": "iOS client"
        ]
        TrebelConnectLog.log("join room \(dictionary as NSDictionary)")
        self.socket.emit("join-room", dictionary)
    }
}

// MARK: - Public methods
extension SocketClient {
    func connect() {
        self.socket.connect()
    }

    func disconnect(completion: (() -> Void)?) {
        self.socket.emit("disconnect", self.room) { [weak self] in
            self?.socket.disconnect()
        }
    }

    func send(event: String, sdp rtcSdp: RTCSessionDescription, to: String) {
        let message = SessionDescription(from: rtcSdp)
        do {
            let data = try JSONEncoder().encode(message)
            let dictionary = (try JSONSerialization.jsonObject(with: data) as? [AnyHashable: Any]) ?? [:]
            let final: [String: Any] = [
                "target": to,
                "caller": self.socket.sid ?? "",
                "sdp": dictionary
            ]
            TrebelConnectLog.log("Socket send \(event): target - \(to), caller - \(self.socket.sid ?? "")")
            self.socket.emit(event, final)
        } catch {
            TrebelConnectLog.log("Socket send \(event) error decode: \(error)")
        }
    }

    func send(candidate rtcIceCandidate: RTCIceCandidate, to: String) {
        let candidate = IceCandidate(from: rtcIceCandidate)
        do {
            let data = try JSONEncoder().encode(candidate)
            let dictionary = (try JSONSerialization.jsonObject(with: data) as? [AnyHashable: Any]) ?? [:]
            let final: [String: Any] = [
                "target": to,
                "caller": self.socket.sid ?? "",
                "candidate": dictionary
            ]
            TrebelConnectLog.log("Socket send candidate: target - \(to), caller - \(self.socket.sid ?? "")")
            self.socket.emit("ice-candidate", final)
        } catch {
            TrebelConnectLog.log("Socket send candidate error decode: \(error)")
        }
    }
}
