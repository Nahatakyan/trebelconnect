//
//  TrebelConnectModels.swift
//  TrebelConnect
//
//  Created by Ruben Nahatakyan on 02.06.22.
//

import UIKit

enum TrebelConnectDataType: String {
    case trackInfo = "track_info"
    case playerAction = "player_action"
    case playerState = "player_state"
    case playerProgress = "player_progress"
}

// Track info
@objc open class TrebelTrackInfo: NSObject, Codable {
    public let title: String
    public let artist: String
    public let image: String?
    private let imageDataHex: String?
    public let duration: Double

    public var imageData: Data? {
        guard let data = imageDataHex?.hexadecimal else { return nil }
        return data
    }
}

// Player state
struct PlayerStateModel: Codable {
    let state: TrebelPlayerState
}

@objc public enum TrebelPlayerState: Int, Codable {
    case stoped
    case paused
    case loading
    case playing
    case unknown
}

// Player action
struct PlayerAction: Codable {
    let type: PlayerActionType
}

enum PlayerActionType: String, Codable {
    case play
    case pause
    case next
    case previous

    case mostPlayed = "most_play"
    case shuffle = "shuffle_play"
    case recentlyPlayed = "recently_play"
}

// Player progress
struct PlayerProgressModel: Codable {
    let progress: Double
}

// Connection state
@objc public enum TrebelConnectState: Int {
    case disconnected
    case connecting
    case connected
}

// Errors
enum TrebelConnectError: Error {
    case AlreadyConnectedOrConnecting
    case CantOpenTrebelApp
}

@objc public enum TrebelConnectPlayType: Int {
    case shuffle
    case mostPlayed
    case recentlyPlayed
}
