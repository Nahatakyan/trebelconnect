//
//  TrebelConnectLog.swift
//  TrebelConnect
//
//  Created by Ruben Nahatakyan on 02.06.22.
//

import Foundation

class TrebelConnectLog {
    
    private init() { }
    
    class func log(_ items: Any..., separator: String = " ", terminator: String = "\n") {
        guard TrebelConnect.shared.logEnabled else { return }
        debugPrint("TrebelConnect log: ", items, separator: separator, terminator: terminator)
    }
}
