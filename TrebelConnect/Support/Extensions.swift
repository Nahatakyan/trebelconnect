//
//  Extensions.swift
//  TrebelConnect
//
//  Created by Ruben Nahatakyan on 02.06.22.
//

import Foundation

extension Data {
    var text: NSString {
        return NSString(data: self, encoding: String.Encoding.utf8.rawValue) ?? "Can't get text from data"
    }

    var hexadecimal: String {
        return map { String(format: "%02x", $0) }.joined()
    }
}

extension String {
    var hexadecimal: Data? {
        var data = Data(capacity: count / 2)

        let regex = try! NSRegularExpression(pattern: "[0-9a-f]{1,2}", options: .caseInsensitive)
        regex.enumerateMatches(in: self, range: NSRange(startIndex..., in: self)) { match, _, _ in
            let byteString = (self as NSString).substring(with: match!.range)
            let num = UInt8(byteString, radix: 16)!
            data.append(num)
        }

        return data.count > 0 ? data : nil
    }
}
